@extends('layout/principal')
@section('conteudo')
  <h1>Listagem de produtos</h1>
  <div class="container">

    <h1>Detalhes do produto: {{$dtlhe->nome}} </h1>
    <ul>
      <li>
        <b>Valor:</b> R$ {{$dtlhe->valor}} 
      </li>
      <li>
        <b>Descrição:</b> {{$dtlhe->descricao}} 
      </li>
      <li>
        <b>Quantidade em estoque:</b> {{$dtlhe->quantidade}}
      </li>
    </ul>

  </div>
@stop
