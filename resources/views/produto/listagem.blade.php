@extends('layout/principal')
@section('conteudo')

<script>
$.ajax({  
  type: "GET",    
  url: "http://localhost:8000/produtos/json",    
  success: function(prod) {
    tabela = `<table class="table table-striped table-bordered table-hover">`;
    $.each(prod, function(index, value) {
      tabela += `
        <tr>
          <td>
            ${value.name}
          </td>
          <td>
            ${value.description}
          </td>
          <td>
            ${value.free_shipping}
          </td>
          <td>
            ${value.price}
          </td>
          <td><a href="/produtos/update/${value.id}"><span class="glyphicon glyphicon-search">atualiza</span></a></td>          
          <td><a href="#" onclick="rmv.remove(${value.id})"><span class="glyphicon glyphicon-trash">Del</span></a></td>
        </tr>
      `;
    });

    tabela += `</table>`;

    $('#tbproduto').html(tabela);
  }
});

class Rmv {

  remove(vl) {    
    console.log(vl);
    $.ajax({
      type: "DELETE",
      url: "http://localhost:8000/produtos/remove/"+vl,
      success : function(result){
        $(this).html("Success!");
        window.location.href="/produtos";
      },
      error : function(){
          $(this).html("Error!");
          
      }
    });
    
  }
}

let rmv = new Rmv();
$(document).ready(function(){
    rmv.remove();
});

</script>

  <h1>Listagem de produtos</h1>
  
    
  <div id="tbproduto">
      
  

@stop
  