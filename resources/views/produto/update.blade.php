
@extends('layout.principal')
@section('conteudo')

	<h1>atualiza produto</h1>

	<form id="formId">	  
	  <input type="hidden" name="_method" value="PUT"></input> 
	  <div class="form-group">
	    <label>Nome</label>
	    <input name="name" value="{{$dtlhe->name}}" class="form-control"/>
	  </div>
      <div class="form-group">
        <label>lm</label>
        <input name="lm" value="{{$dtlhe->lm}}" class="form-control"/>
      </div>
	  <div class="form-group">
	    <label>Descrição</label>
	    <input name="description" value="{{$dtlhe->description}}" class="form-control"/>
	  </div>
	  <div class="form-group">
	    <label>Valor</label>
	    <input name="price" value="{{$dtlhe->price}}" class="form-control"/>
	  </div>
      <div class="form-group">
        <label>Frete Grátis</label>
        
        <input type="radio" name="free_shipping" value="1" {{$dtlhe->free_shipping ? 'checked' : ''}} >Sim
        <input type="radio" name="free_shipping" value="0" {{$dtlhe->free_shipping ? '' : 'checked'}} >Não
      </div>	  
	  <button type="submit" class="btn btn-primary btn-block">Submit</button>
	</form>

<script>
$( document ).ready(function() {
    $("#formId").submit(function(e){
        //e.preventDefault();        
        var form = $(this);          
        console.log(form.serialize());

        //Salva produto
        $.ajax({
            url : "http://localhost:8000/produtos/atualizar/{{$dtlhe->id}}",
            type: 'PUT',
            dataType : 'json',            
            data: form.serialize(),
            success : function(result){
                $(this).html("Success!");
                
            },
            error : function(){
                $(this).html("Error!");
                
            }
        });
    });
});

    

</script>

@stop