<?php

Route::get('/', function(){

	return "<h1>Laravel Teste PVP</h1>";
});

Route::get('/produtos', 'ProdutoController@lista');

Route::get('produtos/json', 'ProdutoController@listaJson');

Route::get('/produtos/detalhe/{id}', 'ProdutoController@mostra');

Route::delete('/produtos/remove/{id}', 'ProdutoController@remove');

Route::post('produtos/adiciona', 'ProdutoController@adiciona');

Route::put('produtos/atualizar/{id}', 'ProdutoController@atualiza');

Route::post('produtos/create', 'UploadController@create');

Route::get('produtos/import', 'UploadController@import');

Route::post('produtos/postImport', 'UploadController@postImport');

Route::get('produtos/update/{id}', 'ProdutoController@update');
