<?php namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use estoque\Commands\ReadFileCommand;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Facades\Input;
use estoque\User;
//use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use estoque\Produto;

class ProdutoController extends Controller {

    use DispatchesCommands;

    public function lista(){

    	$data = [];

    	$produtos = Produto::all();

    	$data['produtos'] = $produtos;

        return view('produto.listagem', $data);
    }

    public function listaJson(){
    	$prod = Produto::all();

    	return response()->json($prod);
    }    

    public function remove($id){

        $produto = Produto::find($id);
        $produto->delete();

        return redirect('/produtos');
    }

    public function update($id){

        $data = [];

        $produtos = Produto::find($id);

        $data['dtlhe'] = $produtos;

        return view('produto.update', $data);
    }


    public function atualiza($id){

        $data = [];

        $data['atualizado'] = 'ok';

        $parametros = Request::all();
        $atualiza = Produto::find($id);
        $atualiza->update($parametros);

        return redirect('/produtos', $data);

    }

    
    public function adiciona(){

        $params = Request::all();
        $produto = new Produto($params);    
        $produto->save();

        //Produto::create(Request::all());

    	return redirect('/produtos')->withInput(Request::only('nome'));
    }
    
}

