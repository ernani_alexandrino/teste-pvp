<?php namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;
use estoque\Http\Requests;
use estoque\Commands\ReadFileCommand;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use estoque\Produto;

class UploadController extends Controller {

    use DispatchesCommands;

    public function create(Request $request){

        //Guarda arquivo em disco
        //$file = $request->file->storeAs('arq','User.xlsx');
        $file = $request->file('file');
        if(empty($file)){
            abort(400, 'Nenhum arquivo encontrado');
        }

        $path = $file->move('upload','prod.xlsx');
        

        $this->dispatch(
            new ReadFileCommand()
        );

        return redirect('/produtos');
    }

    public function import(){

        return view('upload.import');
    }

    public function postImport(){

        \Excel::load(Input::file('file'),function($reader){
            $reader->each(function($sheet){
                foreach ($sheet->toArray() as $row) {
                    User::firstOrCreate($sheet->toArray());
                }
            });
        });

        echo "Importação Completa";
    }
}

