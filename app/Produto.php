<?php namespace estoque;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model {

	public $timestamps = false;

	protected $fillable = array('lm','name', 'description', 'price', 'free_shipping');

	//

}
